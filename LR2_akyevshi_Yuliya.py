import math   #Подключение математической библиотеки

#Вспомогательные функции

def angle(x1, y1, x2, y2):
    if x2 != x1:
        if x2 > x1 and y2 >= y1:
            alpha = math.atan((y2 - y1)/(x2 - x1))
        elif x2 < x1:
            alpha = math.pi + math.atan((y2 - y1)/(x2 - x1))
        elif x2 != x1:
            alpha = 2 * math.pi + math.atan((y2 - y1)/(x2 - x1))
        alpha = math.degrees(alpha)
    else:
        if y2 > y1:
            alpha = 90
        elif y2 < y1:
            alpha = 270
        else:
            alpha = 0
    return alpha   #Вычисление угла между двумя вершинами графа

def dist(x1, y1, x2, y2):
    dst = math.sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2))
    return dst   #Вычисление расстояния между двумя вершинами графа

def inp():
    coord = []
    id_data = ['0', '1', '2', '3', '4']
    x_data = [0, 10, 50, 100, 100]   #Определение абсцисс вершин графа
    y_data = [25, 10, 10, 25, 50]   #Определение ординат вершин графа
    for i in range(5):
        point = {
            'ID': id_data[i],
            'x': x_data[i],
            'y': y_data[i],
        }
        coord.append(point)
    return coord   #Задание опорных точек карты местности и точек старта/финиша

def route_posled(posled, coord):
    k_fwd = 0.052   #Коэффициент движения прямо
    k_pov = 0.0107   #Коэффициент поворота
    mode = []
    value = []
    value_test = []
    pred_angle = float(input('\nВведите начальный угол робота. (Примеры: смотрит на дверь: 90, на окно: 270, на стену с 221м: 0)\nОжидание ввода: '))
    mode.append(5)   #Retry = 5
    value.append(0)
    value_test.append(0)
    while len(posled) >= 2:
        treb_angle = angle(coord[int(posled[0])]['x'], coord[int(posled[0])]['y'], coord[int(posled[1])]['x'], coord[int(posled[1])]['y'])
        d_angle = treb_angle - pred_angle
        if d_angle <= -180:
            d_angle += 360
        elif d_angle > 180:
            d_angle -= 360
        value_test.append(abs(d_angle))
        value.append(round(abs(d_angle) * 5000 * k_pov))
        pred_angle = treb_angle
        if d_angle >= 0:
            mode.append(2)   #Left = 2
        else:
            mode.append(3)   #Right = 3
        value_test.append(dist(coord[int(posled[0])]['x'], coord[int(posled[0])]['y'], coord[int(posled[1])]['x'], coord[int(posled[1])]['y']))
        value.append(round(dist(coord[int(posled[0])]['x'], coord[int(posled[0])]['y'], coord[int(posled[1])]['x'], coord[int(posled[1])]['y']) * 5000 * k_fwd))
        mode.append(1)   #Fwd = 1
        posled = posled[1:]
    mode.append(0)   #Stop = 0
    value.append(0)
    value_test.append(0)
    while len(mode) < 8:
        mode.append(0)   #Stop = 0
        value.append(0)
        value_test.append(0)
    with open('Mode.txt', 'w') as f:
        f.write(str(mode)[1:-1])
    with open('Value.txt', 'w') as f:
        f.write(str(value)[1:-1])
    with open('Value_test.txt', 'w') as f:
        f.write(str(value_test)[1:-1])
    print('\nYour file is ready!')   #Запись маршрутных единиц в файл 

#Основная программа
coord = inp()   #Определение вершин графа по карте местности и точек старта/финиша
route = ['0', '2', '3', '4'] # путь
route_posled(route, coord)   #Запись команд в файл 