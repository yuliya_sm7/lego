/*
 * LegoLab03.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "LegoLab03".
 *
 * Model version              : 1.4
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Thu Mar 15 16:41:18 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LegoLab03_h_
#define RTW_HEADER_LegoLab03_h_
#include <stddef.h>
#include <string.h>
#ifndef LegoLab03_COMMON_INCLUDES_
# define LegoLab03_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "driver_ev3.h"
#endif                                 /* LegoLab03_COMMON_INCLUDES_ */

#include "LegoLab03_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  int32_T Reset;                       /* '<Root>/Chart' */
  uint8_T is_active_c3_LegoLab03;      /* '<Root>/Chart' */
  uint8_T is_c3_LegoLab03;             /* '<Root>/Chart' */
  uint8_T temporalCounter_i1;          /* '<Root>/Chart' */
} DW_LegoLab03_T;

/* Real-time Model Data Structure */
struct tag_RTM_LegoLab03_T {
  const char_T *errorStatus;
};

/* Block states (auto storage) */
extern DW_LegoLab03_T LegoLab03_DW;

/* Model entry point functions */
extern void LegoLab03_initialize(void);
extern void LegoLab03_step(void);
extern void LegoLab03_terminate(void);

/* Real-time Model object */
extern RT_MODEL_LegoLab03_T *const LegoLab03_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S3>/Data Type Conversion' : Eliminate redundant data type conversion
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LegoLab03'
 * '<S1>'   : 'LegoLab03/Chart'
 * '<S2>'   : 'LegoLab03/Display'
 * '<S3>'   : 'LegoLab03/Display1'
 * '<S4>'   : 'LegoLab03/Motor2'
 * '<S5>'   : 'LegoLab03/Motor3'
 */
#endif                                 /* RTW_HEADER_LegoLab03_h_ */
