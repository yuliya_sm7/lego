/*
 * LegoLab03.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "LegoLab03".
 *
 * Model version              : 1.4
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Thu Mar 15 16:41:18 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "LegoLab03.h"
#include "LegoLab03_private.h"

/* Named constants for Chart: '<Root>/Chart' */
#define LegoLab03_IN_Move2             ((uint8_T)1U)
#define LegoLab03_IN_Move3             ((uint8_T)2U)
#define LegoLab03_IN_Move4             ((uint8_T)3U)
#define LegoLab03_IN_Move5             ((uint8_T)4U)
#define LegoLab03_IN_NO_ACTIVE_CHILD   ((uint8_T)0U)
#define LegoLab03_IN_Rotate1           ((uint8_T)5U)
#define LegoLab03_IN_Rotate2           ((uint8_T)6U)
#define LegoLab03_IN_Rotate3           ((uint8_T)7U)
#define LegoLab03_IN_Rotate4           ((uint8_T)8U)

/* Block states (auto storage) */
DW_LegoLab03_T LegoLab03_DW;

/* Real-time model */
RT_MODEL_LegoLab03_T LegoLab03_M_;
RT_MODEL_LegoLab03_T *const LegoLab03_M = &LegoLab03_M_;

/* Model step function */
void LegoLab03_step(void)
{
  uint8_T rtb_UltrasonicSensor_0;
  int32_T rtb_Encoder1_0;
  int8_T tmp;
  int32_T rtb_M_left;
  int32_T rtb_M_right;
  uint8_T tmp_0;

  /* S-Function (ev3_ultrasonic_sensor): '<Root>/Ultrasonic Sensor' */
  rtb_UltrasonicSensor_0 = getUltrasonicSensorValue(1U);

  /* S-Function (ev3_lcd): '<S2>/LCD' incorporates:
   *  DataTypeConversion: '<S2>/Data Type Conversion'
   *  S-Function (ev3_ultrasonic_sensor): '<Root>/Ultrasonic Sensor'
   */
  tmp_0 = 0U;
  lcdDisplay(rtb_UltrasonicSensor_0, &tmp_0, 1U, 1U);

  /* S-Function (ev3_encoder): '<Root>/Encoder1' */
  rtb_Encoder1_0 = getEncoderValueNoReset(4U);

  /* S-Function (ev3_lcd): '<S3>/LCD' incorporates:
   *  S-Function (ev3_encoder): '<Root>/Encoder1'
   */
  tmp_0 = 0U;
  lcdDisplay(rtb_Encoder1_0, &tmp_0, 1U, 1U);

  /* Chart: '<Root>/Chart' incorporates:
   *  S-Function (ev3_encoder): '<Root>/Encoder1'
   *  S-Function (ev3_ultrasonic_sensor): '<Root>/Ultrasonic Sensor'
   */
  if (LegoLab03_DW.temporalCounter_i1 < 31U) {
    LegoLab03_DW.temporalCounter_i1++;
  }

  /* Gateway: Chart */
  /* During: Chart */
  if (LegoLab03_DW.is_active_c3_LegoLab03 == 0U) {
    /* Entry: Chart */
    LegoLab03_DW.is_active_c3_LegoLab03 = 1U;

    /* Entry Internal: Chart */
    /* Transition: '<S1>:25' */
    LegoLab03_DW.is_c3_LegoLab03 = LegoLab03_IN_Move2;

    /* Entry 'Move2': '<S1>:19' */
    /* '<S1>:19:1' M_left = -50; */
    rtb_M_left = -50;

    /* '<S1>:19:1' M_right = -50; */
    rtb_M_right = -50;
  } else {
    switch (LegoLab03_DW.is_c3_LegoLab03) {
     case LegoLab03_IN_Move2:
      rtb_M_left = -50;
      rtb_M_right = -50;

      /* During 'Move2': '<S1>:19' */
      /* '<S1>:22:1' sf_internal_predicateOutput = ... */
      /* '<S1>:22:1' Sensor <15; */
      if (rtb_UltrasonicSensor_0 < 15) {
        /* Transition: '<S1>:22' */
        LegoLab03_DW.is_c3_LegoLab03 = LegoLab03_IN_Rotate1;

        /* Entry 'Rotate1': '<S1>:20' */
        /* '<S1>:20:1' Reset  = EncoderD; */
        LegoLab03_DW.Reset = rtb_Encoder1_0;

        /* '<S1>:20:1' M_left = -50; */
        /* '<S1>:20:3' M_right = 50; */
        rtb_M_right = 50;
      }
      break;

     case LegoLab03_IN_Move3:
      rtb_M_left = -50;
      rtb_M_right = -50;

      /* During 'Move3': '<S1>:28' */
      /* '<S1>:27:1' sf_internal_predicateOutput = ... */
      /* '<S1>:27:1' after(3,sec); */
      if (LegoLab03_DW.temporalCounter_i1 >= 30U) {
        /* Transition: '<S1>:27' */
        LegoLab03_DW.is_c3_LegoLab03 = LegoLab03_IN_Rotate2;

        /* Entry 'Rotate2': '<S1>:30' */
        /* '<S1>:30:1' M_left = 50; */
        rtb_M_left = 50;

        /* '<S1>:30:1' M_right = -50; */
        /* '<S1>:30:1' Reset = EncoderD */
        LegoLab03_DW.Reset = rtb_Encoder1_0;
      }
      break;

     case LegoLab03_IN_Move4:
      rtb_M_left = -50;
      rtb_M_right = -50;

      /* During 'Move4': '<S1>:33' */
      /* '<S1>:37:1' sf_internal_predicateOutput = ... */
      /* '<S1>:37:1' after(3,sec); */
      if (LegoLab03_DW.temporalCounter_i1 >= 30U) {
        /* Transition: '<S1>:37' */
        LegoLab03_DW.is_c3_LegoLab03 = LegoLab03_IN_Rotate3;

        /* Entry 'Rotate3': '<S1>:36' */
        /* '<S1>:36:1' M_left = 50; */
        rtb_M_left = 50;

        /* '<S1>:36:1' M_right = -50; */
        /* '<S1>:36:1' Reset = EncoderD */
        LegoLab03_DW.Reset = rtb_Encoder1_0;
      }
      break;

     case LegoLab03_IN_Move5:
      rtb_M_left = -50;
      rtb_M_right = -50;

      /* During 'Move5': '<S1>:38' */
      /* '<S1>:43:1' sf_internal_predicateOutput = ... */
      /* '<S1>:43:1' after(3,sec); */
      if (LegoLab03_DW.temporalCounter_i1 >= 30U) {
        /* Transition: '<S1>:43' */
        LegoLab03_DW.is_c3_LegoLab03 = LegoLab03_IN_Rotate4;

        /* Entry 'Rotate4': '<S1>:40' */
        /* '<S1>:40:1' M_left = -50; */
        /* '<S1>:40:1' M_right = 50; */
        rtb_M_right = 50;

        /* '<S1>:40:1' Reset = EncoderD */
        LegoLab03_DW.Reset = rtb_Encoder1_0;
      }
      break;

     case LegoLab03_IN_Rotate1:
      rtb_M_left = -50;
      rtb_M_right = 50;

      /* During 'Rotate1': '<S1>:20' */
      /* '<S1>:29:1' sf_internal_predicateOutput = ... */
      /* '<S1>:29:1' (EncoderD - Reset) > 310; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab03_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_0 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab03_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_0 = MIN_int32_T;
      } else {
        rtb_Encoder1_0 -= LegoLab03_DW.Reset;
      }

      if (rtb_Encoder1_0 > 310) {
        /* Transition: '<S1>:29' */
        LegoLab03_DW.is_c3_LegoLab03 = LegoLab03_IN_Move3;
        LegoLab03_DW.temporalCounter_i1 = 0U;

        /* Entry 'Move3': '<S1>:28' */
        /* '<S1>:28:1' M_left = -50; */
        /* '<S1>:28:1' M_right = -50; */
        rtb_M_right = -50;
      }
      break;

     case LegoLab03_IN_Rotate2:
      rtb_M_left = 50;
      rtb_M_right = -50;

      /* During 'Rotate2': '<S1>:30' */
      /* '<S1>:34:1' sf_internal_predicateOutput = ... */
      /* '<S1>:34:1' (EncoderD - Reset) < -310; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab03_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_0 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab03_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_0 = MIN_int32_T;
      } else {
        rtb_Encoder1_0 -= LegoLab03_DW.Reset;
      }

      if (rtb_Encoder1_0 < -310) {
        /* Transition: '<S1>:34' */
        LegoLab03_DW.is_c3_LegoLab03 = LegoLab03_IN_Move4;
        LegoLab03_DW.temporalCounter_i1 = 0U;

        /* Entry 'Move4': '<S1>:33' */
        /* '<S1>:33:1' M_left = -50; */
        rtb_M_left = -50;

        /* '<S1>:33:1' M_right = -50; */
      }
      break;

     case LegoLab03_IN_Rotate3:
      rtb_M_left = 50;
      rtb_M_right = -50;

      /* During 'Rotate3': '<S1>:36' */
      /* '<S1>:39:1' sf_internal_predicateOutput = ... */
      /* '<S1>:39:1' (EncoderD - Reset) < -310; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab03_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_0 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab03_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_0 = MIN_int32_T;
      } else {
        rtb_Encoder1_0 -= LegoLab03_DW.Reset;
      }

      if (rtb_Encoder1_0 < -310) {
        /* Transition: '<S1>:39' */
        LegoLab03_DW.is_c3_LegoLab03 = LegoLab03_IN_Move5;
        LegoLab03_DW.temporalCounter_i1 = 0U;

        /* Entry 'Move5': '<S1>:38' */
        /* '<S1>:38:1' M_left = -50; */
        rtb_M_left = -50;

        /* '<S1>:38:1' M_right = -50; */
      }
      break;

     default:
      rtb_M_left = -50;
      rtb_M_right = 50;

      /* During 'Rotate4': '<S1>:40' */
      /* '<S1>:42:1' sf_internal_predicateOutput = ... */
      /* '<S1>:42:1' (EncoderD - Reset) > 310; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab03_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_0 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab03_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_0 = MIN_int32_T;
      } else {
        rtb_Encoder1_0 -= LegoLab03_DW.Reset;
      }

      if (rtb_Encoder1_0 > 310) {
        /* Transition: '<S1>:42' */
        LegoLab03_DW.is_c3_LegoLab03 = LegoLab03_IN_Move2;

        /* Entry 'Move2': '<S1>:19' */
        /* '<S1>:19:1' M_left = -50; */
        /* '<S1>:19:1' M_right = -50; */
        rtb_M_right = -50;
      }
      break;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* S-Function (ev3_motor): '<S4>/Motor' incorporates:
   *  DataTypeConversion: '<S4>/Data Type Conversion'
   */
  tmp = (int8_T)rtb_M_left;
  setMotor(&tmp, 1U, 2U);

  /* S-Function (ev3_motor): '<S5>/Motor' incorporates:
   *  DataTypeConversion: '<S5>/Data Type Conversion'
   */
  tmp = (int8_T)rtb_M_right;
  setMotor(&tmp, 4U, 2U);

  /* S-Function (ev3_encoder): '<Root>/Encoder' */
  getEncoderValueNoReset(1U);
}

/* Model initialize function */
void LegoLab03_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus(LegoLab03_M, (NULL));

  /* states (dwork) */
  (void) memset((void *)&LegoLab03_DW, 0,
                sizeof(DW_LegoLab03_T));

  /* Start for S-Function (ev3_ultrasonic_sensor): '<Root>/Ultrasonic Sensor' */
  initUltrasonicSensor(1U);

  /* Start for S-Function (ev3_lcd): '<S2>/LCD' */
  initLCD();

  /* Start for S-Function (ev3_encoder): '<Root>/Encoder1' */
  initEncoder(4U);

  /* Start for S-Function (ev3_lcd): '<S3>/LCD' */
  initLCD();

  /* Start for S-Function (ev3_motor): '<S4>/Motor' */
  initMotor(1U);

  /* Start for S-Function (ev3_motor): '<S5>/Motor' */
  initMotor(4U);

  /* Start for S-Function (ev3_encoder): '<Root>/Encoder' */
  initEncoder(1U);

  /* SystemInitialize for Chart: '<Root>/Chart' */
  LegoLab03_DW.temporalCounter_i1 = 0U;
  LegoLab03_DW.is_active_c3_LegoLab03 = 0U;
  LegoLab03_DW.is_c3_LegoLab03 = LegoLab03_IN_NO_ACTIVE_CHILD;
}

/* Model terminate function */
void LegoLab03_terminate(void)
{
  /* Terminate for S-Function (ev3_ultrasonic_sensor): '<Root>/Ultrasonic Sensor' */
  terminateUltrasonicSensor(1U);

  /* Terminate for S-Function (ev3_lcd): '<S2>/LCD' */
  terminateLCD();

  /* Terminate for S-Function (ev3_encoder): '<Root>/Encoder1' */
  terminateEncoder(4U);

  /* Terminate for S-Function (ev3_lcd): '<S3>/LCD' */
  terminateLCD();

  /* Terminate for S-Function (ev3_motor): '<S4>/Motor' */
  terminateMotor(1U, 2U);

  /* Terminate for S-Function (ev3_motor): '<S5>/Motor' */
  terminateMotor(4U, 2U);

  /* Terminate for S-Function (ev3_encoder): '<Root>/Encoder' */
  terminateEncoder(1U);
}
