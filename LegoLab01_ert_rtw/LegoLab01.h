/*
 * LegoLab01.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "LegoLab01".
 *
 * Model version              : 1.2
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Wed Feb 21 15:09:53 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LegoLab01_h_
#define RTW_HEADER_LegoLab01_h_
#include <stddef.h>
#include <string.h>
#ifndef LegoLab01_COMMON_INCLUDES_
# define LegoLab01_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "driver_ev3.h"
#endif                                 /* LegoLab01_COMMON_INCLUDES_ */

#include "LegoLab01_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  uint8_T is_active_c3_LegoLab01;      /* '<Root>/Chart' */
  uint8_T is_c3_LegoLab01;             /* '<Root>/Chart' */
  uint8_T temporalCounter_i1;          /* '<Root>/Chart' */
} DW_LegoLab01_T;

/* Real-time Model Data Structure */
struct tag_RTM_LegoLab01_T {
  const char_T *errorStatus;
};

/* Block states (auto storage) */
extern DW_LegoLab01_T LegoLab01_DW;

/* Model entry point functions */
extern void LegoLab01_initialize(void);
extern void LegoLab01_step(void);
extern void LegoLab01_terminate(void);

/* Real-time Model object */
extern RT_MODEL_LegoLab01_T *const LegoLab01_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LegoLab01'
 * '<S1>'   : 'LegoLab01/Chart'
 * '<S2>'   : 'LegoLab01/Display'
 * '<S3>'   : 'LegoLab01/Motor'
 * '<S4>'   : 'LegoLab01/Motor1'
 * '<S5>'   : 'LegoLab01/Motor2'
 * '<S6>'   : 'LegoLab01/Motor3'
 */
#endif                                 /* RTW_HEADER_LegoLab01_h_ */
