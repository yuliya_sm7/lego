/*
 * LegoLab01_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "LegoLab01".
 *
 * Model version              : 1.2
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Wed Feb 21 15:09:53 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LegoLab01_types_h_
#define RTW_HEADER_LegoLab01_types_h_

/* Forward declaration for rtModel */
typedef struct tag_RTM_LegoLab01_T RT_MODEL_LegoLab01_T;

#endif                                 /* RTW_HEADER_LegoLab01_types_h_ */
