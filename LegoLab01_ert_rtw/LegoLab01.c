/*
 * LegoLab01.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "LegoLab01".
 *
 * Model version              : 1.2
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Wed Feb 21 15:09:53 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "LegoLab01.h"
#include "LegoLab01_private.h"

/* Named constants for Chart: '<Root>/Chart' */
#define LegoLab01_IN_Move              ((uint8_T)1U)
#define LegoLab01_IN_Move1             ((uint8_T)2U)
#define LegoLab01_IN_NO_ACTIVE_CHILD   ((uint8_T)0U)
#define LegoLab01_IN_Rotate            ((uint8_T)3U)
#define LegoLab01_IN_Stop              ((uint8_T)4U)

/* Block states (auto storage) */
DW_LegoLab01_T LegoLab01_DW;

/* Real-time model */
RT_MODEL_LegoLab01_T LegoLab01_M_;
RT_MODEL_LegoLab01_T *const LegoLab01_M = &LegoLab01_M_;

/* Model step function */
void LegoLab01_step(void)
{
  uint8_T rtb_Button_0;
  int8_T tmp;
  int32_T rtb_M_left;
  int32_T rtb_M_right;
  uint8_T tmp_0;

  /* S-Function (ev3_buttons): '<Root>/Button' */
  rtb_Button_0 = getButtonValue(1U);

  /* S-Function (ev3_lcd): '<S2>/LCD' incorporates:
   *  DataTypeConversion: '<S2>/Data Type Conversion'
   *  S-Function (ev3_buttons): '<Root>/Button'
   */
  tmp_0 = 0U;
  lcdDisplay(rtb_Button_0, &tmp_0, 1U, 1U);

  /* Chart: '<Root>/Chart' */
  if (LegoLab01_DW.temporalCounter_i1 < 127U) {
    LegoLab01_DW.temporalCounter_i1++;
  }

  /* Gateway: Chart */
  /* During: Chart */
  if (LegoLab01_DW.is_active_c3_LegoLab01 == 0U) {
    /* Entry: Chart */
    LegoLab01_DW.is_active_c3_LegoLab01 = 1U;

    /* Entry Internal: Chart */
    /* Transition: '<S1>:2' */
    LegoLab01_DW.is_c3_LegoLab01 = LegoLab01_IN_Move;
    LegoLab01_DW.temporalCounter_i1 = 0U;

    /* Entry 'Move': '<S1>:1' */
    /* '<S1>:1:1' M_left = -50; */
    rtb_M_left = -50;

    /* '<S1>:1:1' M_right = -50; */
    rtb_M_right = -50;
  } else {
    switch (LegoLab01_DW.is_c3_LegoLab01) {
     case LegoLab01_IN_Move:
      rtb_M_left = -50;
      rtb_M_right = -50;

      /* During 'Move': '<S1>:1' */
      /* '<S1>:12:1' sf_internal_predicateOutput = ... */
      /* '<S1>:12:1' after(6.4,sec); */
      if (LegoLab01_DW.temporalCounter_i1 >= 64U) {
        /* Transition: '<S1>:12' */
        LegoLab01_DW.is_c3_LegoLab01 = LegoLab01_IN_Rotate;
        LegoLab01_DW.temporalCounter_i1 = 0U;

        /* Entry 'Rotate': '<S1>:10' */
        /* '<S1>:10:1' M_left = -50; */
        /* '<S1>:10:1' M_right = 50; */
        rtb_M_right = 50;
      }
      break;

     case LegoLab01_IN_Move1:
      rtb_M_left = -50;
      rtb_M_right = -50;

      /* During 'Move1': '<S1>:14' */
      /* '<S1>:18:1' sf_internal_predicateOutput = ... */
      /* '<S1>:18:1' after(3.2,sec); */
      if (LegoLab01_DW.temporalCounter_i1 >= 32U) {
        /* Transition: '<S1>:18' */
        LegoLab01_DW.is_c3_LegoLab01 = LegoLab01_IN_Stop;

        /* Entry 'Stop': '<S1>:3' */
        /* '<S1>:3:1' M_left = 0; */
        rtb_M_left = 0;

        /* '<S1>:3:1' M_right = 0; */
        rtb_M_right = 0;
      }
      break;

     case LegoLab01_IN_Rotate:
      rtb_M_left = -50;
      rtb_M_right = 50;

      /* During 'Rotate': '<S1>:10' */
      /* '<S1>:17:1' sf_internal_predicateOutput = ... */
      /* '<S1>:17:1' after(0.9,sec); */
      if (LegoLab01_DW.temporalCounter_i1 >= 9U) {
        /* Transition: '<S1>:17' */
        LegoLab01_DW.is_c3_LegoLab01 = LegoLab01_IN_Move1;
        LegoLab01_DW.temporalCounter_i1 = 0U;

        /* Entry 'Move1': '<S1>:14' */
        /* '<S1>:14:1' M_left = -50; */
        /* '<S1>:14:1' M_right = -50; */
        rtb_M_right = -50;
      }
      break;

     default:
      rtb_M_left = 0;
      rtb_M_right = 0;

      /* During 'Stop': '<S1>:3' */
      break;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* S-Function (ev3_motor): '<S5>/Motor' incorporates:
   *  DataTypeConversion: '<S5>/Data Type Conversion'
   */
  tmp = (int8_T)rtb_M_left;
  setMotor(&tmp, 1U, 2U);

  /* S-Function (ev3_motor): '<S6>/Motor' incorporates:
   *  DataTypeConversion: '<S6>/Data Type Conversion'
   */
  tmp = (int8_T)rtb_M_right;
  setMotor(&tmp, 4U, 2U);
}

/* Model initialize function */
void LegoLab01_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus(LegoLab01_M, (NULL));

  /* states (dwork) */
  (void) memset((void *)&LegoLab01_DW, 0,
                sizeof(DW_LegoLab01_T));

  /* Start for S-Function (ev3_buttons): '<Root>/Button' */
  initButton(1U);

  /* Start for S-Function (ev3_lcd): '<S2>/LCD' */
  initLCD();

  /* Start for S-Function (ev3_motor): '<S5>/Motor' */
  initMotor(1U);

  /* Start for S-Function (ev3_motor): '<S6>/Motor' */
  initMotor(4U);

  /* SystemInitialize for Chart: '<Root>/Chart' */
  LegoLab01_DW.temporalCounter_i1 = 0U;
  LegoLab01_DW.is_active_c3_LegoLab01 = 0U;
  LegoLab01_DW.is_c3_LegoLab01 = LegoLab01_IN_NO_ACTIVE_CHILD;
}

/* Model terminate function */
void LegoLab01_terminate(void)
{
  /* Terminate for S-Function (ev3_buttons): '<Root>/Button' */
  terminateButton(1U);

  /* Terminate for S-Function (ev3_lcd): '<S2>/LCD' */
  terminateLCD();

  /* Terminate for S-Function (ev3_motor): '<S5>/Motor' */
  terminateMotor(1U, 2U);

  /* Terminate for S-Function (ev3_motor): '<S6>/Motor' */
  terminateMotor(4U, 2U);
}
