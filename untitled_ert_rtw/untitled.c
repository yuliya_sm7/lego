/*
 * untitled.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "untitled".
 *
 * Model version              : 1.0
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Sat Apr 14 18:27:18 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "untitled.h"
#include "untitled_private.h"

/* Named constants for Chart: '<Root>/Chart' */
#define untitled_IN_Finish             ((uint8_T)1U)
#define untitled_IN_Left               ((uint8_T)2U)
#define untitled_IN_NO_ACTIVE_CHILD    ((uint8_T)0U)
#define untitled_IN_Right              ((uint8_T)3U)
#define untitled_IN_Rotate             ((uint8_T)4U)
#define untitled_IN_Start              ((uint8_T)5U)
#define untitled_IN_Vpered             ((uint8_T)6U)

/* Block signals (default storage) */
B_untitled_T untitled_B;

/* Block states (default storage) */
DW_untitled_T untitled_DW;

/* Real-time model */
RT_MODEL_untitled_T untitled_M_;
RT_MODEL_untitled_T *const untitled_M = &untitled_M_;

/* Model step function */
void untitled_step(void)
{
  int32_T rtb_Encoder1_0;
  int8_T tmp;
  uint8_T tmp_0;
  real_T tmp_1;
  int32_T tmp_2;

  /* S-Function (ev3_encoder): '<Root>/Encoder1' */
  rtb_Encoder1_0 = getEncoderValueNoReset(4U);

  /* FromWorkspace: '<Root>/From Workspace1' */
  {
    real_T *pDataValues = (real_T *) untitled_DW.FromWorkspace1_PWORK.DataPtr;

    {
      int_T elIdx;
      for (elIdx = 0; elIdx < 7; ++elIdx) {
        (&untitled_B.FromWorkspace1[0])[elIdx] = pDataValues[0];
        pDataValues += 1;
      }
    }
  }

  /* Chart: '<Root>/Chart' incorporates:
   *  S-Function (ev3_encoder): '<Root>/Encoder1'
   */
  if (untitled_DW.temporalCounter_i1 < 63U) {
    untitled_DW.temporalCounter_i1++;
  }

  /* Gateway: Chart */
  /* During: Chart */
  if (untitled_DW.is_active_c3_untitled == 0U) {
    /* Entry: Chart */
    untitled_DW.is_active_c3_untitled = 1U;

    /* Entry Internal: Chart */
    /* Transition: '<S1>:153' */
    untitled_DW.is_c3_untitled = untitled_IN_Start;
    untitled_DW.temporalCounter_i1 = 0U;

    /* Entry 'Start': '<S1>:147' */
    /* '<S1>:147:1' V=-30; */
    untitled_DW.V = -30.0;

    /* '<S1>:147:1' T=-30; */
    untitled_DW.T = -30.0;

    /* '<S1>:147:1' Mot_A=0; */
    untitled_B.Mot_A = 0.0;

    /* '<S1>:147:1' Mot_D=0; */
    untitled_B.Mot_D = 0.0;

    /* '<S1>:147:3' i=1; */
    untitled_DW.i = 1.0;

    /* '<S1>:147:4' value = 0; */
    untitled_B.value = 0.0;
  } else {
    switch (untitled_DW.is_c3_untitled) {
     case untitled_IN_Finish:
      /* During 'Finish': '<S1>:164' */
      break;

     case untitled_IN_Left:
      /* During 'Left': '<S1>:156' */
      /* '<S1>:161:1' sf_internal_predicateOutput = ... */
      /* '<S1>:161:1' (ResetD - EncoderD) < -value; */
      if ((untitled_DW.ResetD >= 0) && (rtb_Encoder1_0 < untitled_DW.ResetD -
           MAX_int32_T)) {
        tmp_2 = MAX_int32_T;
      } else if ((untitled_DW.ResetD < 0) && (rtb_Encoder1_0 >
                  untitled_DW.ResetD - MIN_int32_T)) {
        tmp_2 = MIN_int32_T;
      } else {
        tmp_2 = untitled_DW.ResetD - rtb_Encoder1_0;
      }

      if (tmp_2 < -untitled_B.value) {
        /* Transition: '<S1>:161' */
        untitled_DW.is_c3_untitled = untitled_IN_Vpered;

        /* Entry 'Vpered': '<S1>:154' */
        /* '<S1>:154:1' Mot_A=V; */
        untitled_B.Mot_A = untitled_DW.V;

        /* '<S1>:154:1' Mot_D=V; */
        untitled_B.Mot_D = untitled_DW.V;

        /* '<S1>:154:1' ResetD = EncoderD; */
        untitled_DW.ResetD = rtb_Encoder1_0;

        /* '<S1>:154:3' value = v_out(i); */
        untitled_B.value = untitled_B.FromWorkspace1[(int32_T)untitled_DW.i - 1];

        /* '<S1>:154:5' i = i+1; */
        untitled_DW.i++;
      }
      break;

     case untitled_IN_Right:
      /* During 'Right': '<S1>:157' */
      /* '<S1>:162:1' sf_internal_predicateOutput = ... */
      /* '<S1>:162:1' (ResetD - EncoderD) > -value; */
      if ((untitled_DW.ResetD >= 0) && (rtb_Encoder1_0 < untitled_DW.ResetD -
           MAX_int32_T)) {
        tmp_2 = MAX_int32_T;
      } else if ((untitled_DW.ResetD < 0) && (rtb_Encoder1_0 >
                  untitled_DW.ResetD - MIN_int32_T)) {
        tmp_2 = MIN_int32_T;
      } else {
        tmp_2 = untitled_DW.ResetD - rtb_Encoder1_0;
      }

      if (tmp_2 > -untitled_B.value) {
        /* Transition: '<S1>:162' */
        untitled_DW.is_c3_untitled = untitled_IN_Vpered;

        /* Entry 'Vpered': '<S1>:154' */
        /* '<S1>:154:1' Mot_A=V; */
        untitled_B.Mot_A = untitled_DW.V;

        /* '<S1>:154:1' Mot_D=V; */
        untitled_B.Mot_D = untitled_DW.V;

        /* '<S1>:154:1' ResetD = EncoderD; */
        untitled_DW.ResetD = rtb_Encoder1_0;

        /* '<S1>:154:3' value = v_out(i); */
        untitled_B.value = untitled_B.FromWorkspace1[(int32_T)untitled_DW.i - 1];

        /* '<S1>:154:5' i = i+1; */
        untitled_DW.i++;
      }
      break;

     case untitled_IN_Rotate:
      /* During 'Rotate': '<S1>:146' */
      /* '<S1>:170:1' sf_internal_predicateOutput = ... */
      /* '<S1>:170:1' i > length(v_out); */
      if (untitled_DW.i > 7.0) {
        /* Transition: '<S1>:170' */
        untitled_DW.is_c3_untitled = untitled_IN_Finish;

        /* Entry 'Finish': '<S1>:164' */
        /* '<S1>:164:1' Mot_A=0; */
        untitled_B.Mot_A = 0.0;

        /* '<S1>:164:1' Mot_D=0; */
        untitled_B.Mot_D = 0.0;
      } else {
        /* '<S1>:158:1' sf_internal_predicateOutput = ... */
        /* '<S1>:158:1' value >= 0; */
        if (untitled_B.value >= 0.0) {
          /* Transition: '<S1>:158' */
          untitled_DW.is_c3_untitled = untitled_IN_Left;

          /* Entry 'Left': '<S1>:156' */
          /* '<S1>:156:1' ResetD = EncoderD; */
          untitled_DW.ResetD = rtb_Encoder1_0;

          /* '<S1>:156:1' Mot_A=-T; */
          untitled_B.Mot_A = -untitled_DW.T;

          /* '<S1>:156:1' Mot_D=T; */
          untitled_B.Mot_D = untitled_DW.T;
        } else {
          /* '<S1>:159:1' sf_internal_predicateOutput = ... */
          /* '<S1>:159:1' value < 0; */
          if (untitled_B.value < 0.0) {
            /* Transition: '<S1>:159' */
            untitled_DW.is_c3_untitled = untitled_IN_Right;

            /* Entry 'Right': '<S1>:157' */
            /* '<S1>:157:1' ResetD = EncoderD; */
            untitled_DW.ResetD = rtb_Encoder1_0;

            /* '<S1>:157:1' Mot_A=T; */
            untitled_B.Mot_A = untitled_DW.T;

            /* '<S1>:157:1' Mot_D=-T; */
            untitled_B.Mot_D = -untitled_DW.T;
          }
        }
      }
      break;

     case untitled_IN_Start:
      /* During 'Start': '<S1>:147' */
      /* '<S1>:150:1' sf_internal_predicateOutput = ... */
      /* '<S1>:150:1' after(5, sec); */
      if (untitled_DW.temporalCounter_i1 >= 50U) {
        /* Transition: '<S1>:150' */
        untitled_DW.is_c3_untitled = untitled_IN_Rotate;

        /* Entry 'Rotate': '<S1>:146' */
        /* '<S1>:146:1' value = v_out(i); */
        untitled_B.value = untitled_B.FromWorkspace1[(int32_T)untitled_DW.i - 1];

        /* '<S1>:146:1' i = i+1; */
        untitled_DW.i++;
      }
      break;

     default:
      /* During 'Vpered': '<S1>:154' */
      /* '<S1>:171:1' sf_internal_predicateOutput = ... */
      /* '<S1>:171:1' i > length(v_out); */
      if (untitled_DW.i > 7.0) {
        /* Transition: '<S1>:171' */
        untitled_DW.is_c3_untitled = untitled_IN_Finish;

        /* Entry 'Finish': '<S1>:164' */
        /* '<S1>:164:1' Mot_A=0; */
        untitled_B.Mot_A = 0.0;

        /* '<S1>:164:1' Mot_D=0; */
        untitled_B.Mot_D = 0.0;
      } else {
        /* '<S1>:155:1' sf_internal_predicateOutput = ... */
        /* '<S1>:155:1' (ResetD-EncoderD)  > value; */
        if ((untitled_DW.ResetD >= 0) && (rtb_Encoder1_0 < untitled_DW.ResetD -
             MAX_int32_T)) {
          tmp_2 = MAX_int32_T;
        } else if ((untitled_DW.ResetD < 0) && (rtb_Encoder1_0 >
                    untitled_DW.ResetD - MIN_int32_T)) {
          tmp_2 = MIN_int32_T;
        } else {
          tmp_2 = untitled_DW.ResetD - rtb_Encoder1_0;
        }

        if (tmp_2 > untitled_B.value) {
          /* Transition: '<S1>:155' */
          untitled_DW.is_c3_untitled = untitled_IN_Rotate;

          /* Entry 'Rotate': '<S1>:146' */
          /* '<S1>:146:1' value = v_out(i); */
          untitled_B.value = untitled_B.FromWorkspace1[(int32_T)untitled_DW.i -
            1];

          /* '<S1>:146:1' i = i+1; */
          untitled_DW.i++;
        }
      }
      break;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* S-Function (ev3_lcd): '<S2>/LCD' */
  tmp_0 = 0U;

  /* DataTypeConversion: '<S2>/Data Type Conversion' */
  tmp_1 = floor(untitled_B.value);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 4.294967296E+9);
  }

  /* S-Function (ev3_lcd): '<S2>/LCD' incorporates:
   *  DataTypeConversion: '<S2>/Data Type Conversion'
   */
  lcdDisplay(tmp_1 < 0.0 ? -(int32_T)(uint32_T)-tmp_1 : (int32_T)(uint32_T)tmp_1,
             &tmp_0, 1U, 1U);

  /* DataTypeConversion: '<S3>/Data Type Conversion' */
  tmp_1 = floor(untitled_B.Mot_D);
  if (tmp_1 < 128.0) {
    if (tmp_1 >= -128.0) {
      /* S-Function (ev3_motor): '<S3>/Motor' */
      tmp = (int8_T)tmp_1;
    } else {
      /* S-Function (ev3_motor): '<S3>/Motor' */
      tmp = MIN_int8_T;
    }
  } else {
    /* S-Function (ev3_motor): '<S3>/Motor' */
    tmp = MAX_int8_T;
  }

  /* End of DataTypeConversion: '<S3>/Data Type Conversion' */

  /* S-Function (ev3_motor): '<S3>/Motor' */
  setMotor(&tmp, 1U, 2U);

  /* DataTypeConversion: '<S4>/Data Type Conversion' */
  tmp_1 = floor(untitled_B.Mot_A);
  if (tmp_1 < 128.0) {
    if (tmp_1 >= -128.0) {
      /* S-Function (ev3_motor): '<S4>/Motor' */
      tmp = (int8_T)tmp_1;
    } else {
      /* S-Function (ev3_motor): '<S4>/Motor' */
      tmp = MIN_int8_T;
    }
  } else {
    /* S-Function (ev3_motor): '<S4>/Motor' */
    tmp = MAX_int8_T;
  }

  /* End of DataTypeConversion: '<S4>/Data Type Conversion' */

  /* S-Function (ev3_motor): '<S4>/Motor' */
  setMotor(&tmp, 4U, 2U);

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++untitled_M->Timing.clockTick0)) {
    ++untitled_M->Timing.clockTickH0;
  }

  untitled_M->Timing.t[0] = untitled_M->Timing.clockTick0 *
    untitled_M->Timing.stepSize0 + untitled_M->Timing.clockTickH0 *
    untitled_M->Timing.stepSize0 * 4294967296.0;

  {
    /* Update absolute timer for sample time: [0.1s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The resolution of this integer timer is 0.1, which is the step size
     * of the task. Size of "clockTick1" ensures timer will not overflow during the
     * application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    untitled_M->Timing.clockTick1++;
    if (!untitled_M->Timing.clockTick1) {
      untitled_M->Timing.clockTickH1++;
    }
  }
}

/* Model initialize function */
void untitled_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)untitled_M, 0,
                sizeof(RT_MODEL_untitled_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&untitled_M->solverInfo,
                          &untitled_M->Timing.simTimeStep);
    rtsiSetTPtr(&untitled_M->solverInfo, &rtmGetTPtr(untitled_M));
    rtsiSetStepSizePtr(&untitled_M->solverInfo, &untitled_M->Timing.stepSize0);
    rtsiSetErrorStatusPtr(&untitled_M->solverInfo, (&rtmGetErrorStatus
      (untitled_M)));
    rtsiSetRTModelPtr(&untitled_M->solverInfo, untitled_M);
  }

  rtsiSetSimTimeStep(&untitled_M->solverInfo, MAJOR_TIME_STEP);
  rtsiSetSolverName(&untitled_M->solverInfo,"FixedStepDiscrete");
  rtmSetTPtr(untitled_M, &untitled_M->Timing.tArray[0]);
  untitled_M->Timing.stepSize0 = 0.1;

  /* block I/O */
  (void) memset(((void *) &untitled_B), 0,
                sizeof(B_untitled_T));

  /* states (dwork) */
  (void) memset((void *)&untitled_DW, 0,
                sizeof(DW_untitled_T));

  /* Start for S-Function (ev3_encoder): '<Root>/Encoder1' */
  initEncoder(4U);

  /* Start for FromWorkspace: '<Root>/From Workspace1' */
  {
    static real_T pTimeValues0[] = { 0.0 } ;

    static real_T pDataValues0[] = { -421.08730403779435, 824.62112512353212,
      421.08730403779435, 4200.0, 654.04228459055435, 1077.0329614269008, 0.0 } ;

    untitled_DW.FromWorkspace1_PWORK.TimePtr = (void *) pTimeValues0;
    untitled_DW.FromWorkspace1_PWORK.DataPtr = (void *) pDataValues0;
    untitled_DW.FromWorkspace1_IWORK.PrevIndex = 0;
  }

  /* Start for S-Function (ev3_lcd): '<S2>/LCD' */
  initLCD();

  /* Start for S-Function (ev3_motor): '<S3>/Motor' */
  initMotor(1U);

  /* Start for S-Function (ev3_motor): '<S4>/Motor' */
  initMotor(4U);

  /* SystemInitialize for Chart: '<Root>/Chart' */
  untitled_DW.temporalCounter_i1 = 0U;
  untitled_DW.is_active_c3_untitled = 0U;
  untitled_DW.is_c3_untitled = untitled_IN_NO_ACTIVE_CHILD;
}

/* Model terminate function */
void untitled_terminate(void)
{
  /* Terminate for S-Function (ev3_encoder): '<Root>/Encoder1' */
  terminateEncoder(4U);

  /* Terminate for S-Function (ev3_lcd): '<S2>/LCD' */
  terminateLCD();

  /* Terminate for S-Function (ev3_motor): '<S3>/Motor' */
  terminateMotor(1U, 2U);

  /* Terminate for S-Function (ev3_motor): '<S4>/Motor' */
  terminateMotor(4U, 2U);
}
