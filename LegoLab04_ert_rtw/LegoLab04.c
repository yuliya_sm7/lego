/*
 * LegoLab04.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "LegoLab04".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Apr 11 12:25:09 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "LegoLab04.h"
#include "LegoLab04_private.h"

/* Named constants for Chart: '<Root>/Chart' */
#define LegoLab04_IN_Move              ((uint8_T)1U)
#define LegoLab04_IN_Move3             ((uint8_T)2U)
#define LegoLab04_IN_Move4             ((uint8_T)3U)
#define LegoLab04_IN_Move5             ((uint8_T)4U)
#define LegoLab04_IN_Move6             ((uint8_T)5U)
#define LegoLab04_IN_Move7             ((uint8_T)6U)
#define LegoLab04_IN_NO_ACTIVE_CHILD   ((uint8_T)0U)
#define LegoLab04_IN_Rotate            ((uint8_T)7U)
#define LegoLab04_IN_Rotate1           ((uint8_T)8U)
#define LegoLab04_IN_Rotate2           ((uint8_T)9U)
#define LegoLab04_IN_Rotate3           ((uint8_T)10U)
#define LegoLab04_IN_Rotate4           ((uint8_T)11U)
#define LegoLab04_IN_Rotate5           ((uint8_T)12U)
#define LegoLab04_IN_Stop              ((uint8_T)13U)

/* Block signals (default storage) */
B_LegoLab04_T LegoLab04_B;

/* Block states (default storage) */
DW_LegoLab04_T LegoLab04_DW;

/* Real-time model */
RT_MODEL_LegoLab04_T LegoLab04_M_;
RT_MODEL_LegoLab04_T *const LegoLab04_M = &LegoLab04_M_;

/* Model step function */
void LegoLab04_step(void)
{
  uint8_T rtb_UltrasonicSensor_0;
  int32_T rtb_Encoder1_0;
  int8_T tmp;
  real_T tmp_0;
  int32_T rtb_Encoder1_1;

  /* S-Function (ev3_ultrasonic_sensor): '<Root>/Ultrasonic Sensor' */
  rtb_UltrasonicSensor_0 = getUltrasonicSensorValue(1U);

  /* S-Function (ev3_lcd): '<S2>/LCD' incorporates:
   *  DataTypeConversion: '<S2>/Data Type Conversion'
   *  S-Function (ev3_ultrasonic_sensor): '<Root>/Ultrasonic Sensor'
   */
  lcdDisplay(rtb_UltrasonicSensor_0, LegoLab04_ConstP.LCD_p1, 1U, 1U);

  /* S-Function (ev3_encoder): '<Root>/Encoder1' */
  rtb_Encoder1_0 = getEncoderValueNoReset(4U);

  /* S-Function (ev3_lcd): '<S3>/LCD' incorporates:
   *  S-Function (ev3_encoder): '<Root>/Encoder1'
   */
  lcdDisplay(rtb_Encoder1_0, LegoLab04_ConstP.LCD_p1_m, 1U, 1U);

  /* Chart: '<Root>/Chart' incorporates:
   *  S-Function (ev3_encoder): '<Root>/Encoder1'
   */
  if (LegoLab04_DW.temporalCounter_i1 < 31U) {
    LegoLab04_DW.temporalCounter_i1++;
  }

  /* Gateway: Chart */
  /* During: Chart */
  if (LegoLab04_DW.is_active_c3_LegoLab04 == 0U) {
    /* Entry: Chart */
    LegoLab04_DW.is_active_c3_LegoLab04 = 1U;

    /* Entry Internal: Chart */
    /* Transition: '<S1>:2' */
    LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Move;

    /* Entry 'Move': '<S1>:1' */
    /* '<S1>:1:1' Reset = EncoderD; */
    LegoLab04_DW.Reset = rtb_Encoder1_0;

    /* '<S1>:1:1' M_left = -V; */
    LegoLab04_B.M_left = -LegoLab04_DW.V;

    /* '<S1>:1:1' M_right = -V; */
    LegoLab04_B.M_right = -LegoLab04_DW.V;
  } else {
    switch (LegoLab04_DW.is_c3_LegoLab04) {
     case LegoLab04_IN_Move:
      /* During 'Move': '<S1>:1' */
      /* '<S1>:12:1' sf_internal_predicateOutput = ... */
      /* '<S1>:12:1' (EncoderD - Reset)  < -3000; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab04_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_1 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab04_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_1 = MIN_int32_T;
      } else {
        rtb_Encoder1_1 = rtb_Encoder1_0 - LegoLab04_DW.Reset;
      }

      if (rtb_Encoder1_1 < -3000) {
        /* Transition: '<S1>:12' */
        LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Rotate;

        /* Entry 'Rotate': '<S1>:10' */
        /* '<S1>:10:1' M_left = -V; */
        LegoLab04_B.M_left = -LegoLab04_DW.V;

        /* '<S1>:10:1' M_right = V; */
        LegoLab04_B.M_right = LegoLab04_DW.V;

        /* '<S1>:10:1' Reset = EncoderD; */
        LegoLab04_DW.Reset = rtb_Encoder1_0;
      }
      break;

     case LegoLab04_IN_Move3:
      /* During 'Move3': '<S1>:28' */
      /* '<S1>:27:1' sf_internal_predicateOutput = ... */
      /* '<S1>:27:1' after(3,sec); */
      if (LegoLab04_DW.temporalCounter_i1 >= 30U) {
        /* Transition: '<S1>:27' */
        LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Rotate2;

        /* Entry 'Rotate2': '<S1>:30' */
        /* '<S1>:30:1' M_left = V; */
        LegoLab04_B.M_left = LegoLab04_DW.V;

        /* '<S1>:30:1' M_right = -V; */
        LegoLab04_B.M_right = -LegoLab04_DW.V;

        /* '<S1>:30:1' Reset = EncoderD */
        LegoLab04_DW.Reset = rtb_Encoder1_0;
      }
      break;

     case LegoLab04_IN_Move4:
      /* During 'Move4': '<S1>:33' */
      /* '<S1>:37:1' sf_internal_predicateOutput = ... */
      /* '<S1>:37:1' after(3,sec); */
      if (LegoLab04_DW.temporalCounter_i1 >= 30U) {
        /* Transition: '<S1>:37' */
        LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Rotate3;

        /* Entry 'Rotate3': '<S1>:36' */
        /* '<S1>:36:1' M_left = V; */
        LegoLab04_B.M_left = LegoLab04_DW.V;

        /* '<S1>:36:1' M_right = -V; */
        LegoLab04_B.M_right = -LegoLab04_DW.V;

        /* '<S1>:36:1' Reset = EncoderD */
        LegoLab04_DW.Reset = rtb_Encoder1_0;
      }
      break;

     case LegoLab04_IN_Move5:
      /* During 'Move5': '<S1>:38' */
      /* '<S1>:43:1' sf_internal_predicateOutput = ... */
      /* '<S1>:43:1' after(3,sec); */
      if (LegoLab04_DW.temporalCounter_i1 >= 30U) {
        /* Transition: '<S1>:43' */
        LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Rotate4;

        /* Entry 'Rotate4': '<S1>:40' */
        /* '<S1>:40:1' M_left = -V; */
        LegoLab04_B.M_left = -LegoLab04_DW.V;

        /* '<S1>:40:1' M_right = V; */
        LegoLab04_B.M_right = LegoLab04_DW.V;

        /* '<S1>:40:1' Reset = EncoderD */
        LegoLab04_DW.Reset = rtb_Encoder1_0;
      }
      break;

     case LegoLab04_IN_Move6:
      /* During 'Move6': '<S1>:55' */
      /* '<S1>:59:1' sf_internal_predicateOutput = ... */
      /* '<S1>:59:1' (EncoderD - Reset) < -3000; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab04_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_1 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab04_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_1 = MIN_int32_T;
      } else {
        rtb_Encoder1_1 = rtb_Encoder1_0 - LegoLab04_DW.Reset;
      }

      if (rtb_Encoder1_1 < -3000) {
        /* Transition: '<S1>:59' */
        LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Rotate5;

        /* Entry 'Rotate5': '<S1>:60' */
        /* '<S1>:60:1' M_left = -V; */
        LegoLab04_B.M_left = -LegoLab04_DW.V;

        /* '<S1>:60:1' M_right = V; */
        LegoLab04_B.M_right = LegoLab04_DW.V;

        /* '<S1>:60:1' Reset = EncoderD; */
        LegoLab04_DW.Reset = rtb_Encoder1_0;
      }
      break;

     case LegoLab04_IN_Move7:
      /* During 'Move7': '<S1>:61' */
      /* '<S1>:70:1' sf_internal_predicateOutput = ... */
      /* '<S1>:70:1' (EncoderD - Reset) < -3000; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab04_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_0 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab04_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_0 = MIN_int32_T;
      } else {
        rtb_Encoder1_0 -= LegoLab04_DW.Reset;
      }

      if (rtb_Encoder1_0 < -3000) {
        /* Transition: '<S1>:70' */
        LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Stop;

        /* Entry 'Stop': '<S1>:68' */
        /* '<S1>:68:1' M_left = 0; */
        LegoLab04_B.M_left = 0.0;

        /* '<S1>:68:1' M_right = 0; */
        LegoLab04_B.M_right = 0.0;
      }
      break;

     case LegoLab04_IN_Rotate:
      /* During 'Rotate': '<S1>:10' */
      /* '<S1>:17:1' sf_internal_predicateOutput = ... */
      /* '<S1>:17:1' (EncoderD - Reset) > 310; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab04_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_1 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab04_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_1 = MIN_int32_T;
      } else {
        rtb_Encoder1_1 = rtb_Encoder1_0 - LegoLab04_DW.Reset;
      }

      if (rtb_Encoder1_1 > 310) {
        /* Transition: '<S1>:17' */
        LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Move6;

        /* Entry 'Move6': '<S1>:55' */
        /* '<S1>:55:1' Reset = EncoderD; */
        LegoLab04_DW.Reset = rtb_Encoder1_0;

        /* '<S1>:55:1' M_left = -V; */
        LegoLab04_B.M_left = -LegoLab04_DW.V;

        /* '<S1>:55:1' M_right = -V; */
        LegoLab04_B.M_right = -LegoLab04_DW.V;
      }
      break;

     case LegoLab04_IN_Rotate1:
      /* During 'Rotate1': '<S1>:20' */
      /* '<S1>:29:1' sf_internal_predicateOutput = ... */
      /* '<S1>:29:1' (EncoderD - Reset) > 310; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab04_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_0 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab04_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_0 = MIN_int32_T;
      } else {
        rtb_Encoder1_0 -= LegoLab04_DW.Reset;
      }

      if (rtb_Encoder1_0 > 310) {
        /* Transition: '<S1>:29' */
        LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Move3;
        LegoLab04_DW.temporalCounter_i1 = 0U;

        /* Entry 'Move3': '<S1>:28' */
        /* '<S1>:28:1' M_left = -V; */
        LegoLab04_B.M_left = -LegoLab04_DW.V;

        /* '<S1>:28:1' M_right = -V; */
        LegoLab04_B.M_right = -LegoLab04_DW.V;
      }
      break;

     case LegoLab04_IN_Rotate2:
      /* During 'Rotate2': '<S1>:30' */
      /* '<S1>:34:1' sf_internal_predicateOutput = ... */
      /* '<S1>:34:1' (EncoderD - Reset) < -310; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab04_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_0 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab04_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_0 = MIN_int32_T;
      } else {
        rtb_Encoder1_0 -= LegoLab04_DW.Reset;
      }

      if (rtb_Encoder1_0 < -310) {
        /* Transition: '<S1>:34' */
        LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Move4;
        LegoLab04_DW.temporalCounter_i1 = 0U;

        /* Entry 'Move4': '<S1>:33' */
        /* '<S1>:33:1' M_left = -V; */
        LegoLab04_B.M_left = -LegoLab04_DW.V;

        /* '<S1>:33:1' M_right = -V; */
        LegoLab04_B.M_right = -LegoLab04_DW.V;
      }
      break;

     case LegoLab04_IN_Rotate3:
      /* During 'Rotate3': '<S1>:36' */
      /* '<S1>:39:1' sf_internal_predicateOutput = ... */
      /* '<S1>:39:1' (EncoderD - Reset) < -310; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab04_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_0 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab04_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_0 = MIN_int32_T;
      } else {
        rtb_Encoder1_0 -= LegoLab04_DW.Reset;
      }

      if (rtb_Encoder1_0 < -310) {
        /* Transition: '<S1>:39' */
        LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Move5;
        LegoLab04_DW.temporalCounter_i1 = 0U;

        /* Entry 'Move5': '<S1>:38' */
        /* '<S1>:38:1' M_left = -V; */
        LegoLab04_B.M_left = -LegoLab04_DW.V;

        /* '<S1>:38:1' M_right = -V; */
        LegoLab04_B.M_right = -LegoLab04_DW.V;
      }
      break;

     case LegoLab04_IN_Rotate4:
      /* During 'Rotate4': '<S1>:40' */
      break;

     case LegoLab04_IN_Rotate5:
      /* During 'Rotate5': '<S1>:60' */
      /* '<S1>:62:1' sf_internal_predicateOutput = ... */
      /* '<S1>:62:1' (EncoderD - Reset) > 310; */
      if ((rtb_Encoder1_0 >= 0) && (LegoLab04_DW.Reset < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_1 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (LegoLab04_DW.Reset > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_1 = MIN_int32_T;
      } else {
        rtb_Encoder1_1 = rtb_Encoder1_0 - LegoLab04_DW.Reset;
      }

      if (rtb_Encoder1_1 > 310) {
        /* Transition: '<S1>:62' */
        LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_Move7;

        /* Entry 'Move7': '<S1>:61' */
        /* '<S1>:61:1' Reset = EncoderD; */
        LegoLab04_DW.Reset = rtb_Encoder1_0;

        /* '<S1>:61:1' M_left = -V; */
        LegoLab04_B.M_left = -LegoLab04_DW.V;

        /* '<S1>:61:1' M_right = -V; */
        LegoLab04_B.M_right = -LegoLab04_DW.V;
      }
      break;

     default:
      /* During 'Stop': '<S1>:68' */
      break;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* DataTypeConversion: '<S4>/Data Type Conversion' */
  tmp_0 = floor(LegoLab04_B.M_left);
  if (tmp_0 < 128.0) {
    if (tmp_0 >= -128.0) {
      /* S-Function (ev3_motor): '<S4>/Motor' */
      tmp = (int8_T)tmp_0;
    } else {
      /* S-Function (ev3_motor): '<S4>/Motor' */
      tmp = MIN_int8_T;
    }
  } else {
    /* S-Function (ev3_motor): '<S4>/Motor' */
    tmp = MAX_int8_T;
  }

  /* End of DataTypeConversion: '<S4>/Data Type Conversion' */

  /* S-Function (ev3_motor): '<S4>/Motor' */
  setMotor(&tmp, 1U, 2U);

  /* DataTypeConversion: '<S5>/Data Type Conversion' */
  tmp_0 = floor(LegoLab04_B.M_right);
  if (tmp_0 < 128.0) {
    if (tmp_0 >= -128.0) {
      /* S-Function (ev3_motor): '<S5>/Motor' */
      tmp = (int8_T)tmp_0;
    } else {
      /* S-Function (ev3_motor): '<S5>/Motor' */
      tmp = MIN_int8_T;
    }
  } else {
    /* S-Function (ev3_motor): '<S5>/Motor' */
    tmp = MAX_int8_T;
  }

  /* End of DataTypeConversion: '<S5>/Data Type Conversion' */

  /* S-Function (ev3_motor): '<S5>/Motor' */
  setMotor(&tmp, 4U, 2U);

  /* S-Function (ev3_encoder): '<Root>/Encoder' */
  getEncoderValueNoReset(1U);
}

/* Model initialize function */
void LegoLab04_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus(LegoLab04_M, (NULL));

  /* block I/O */
  (void) memset(((void *) &LegoLab04_B), 0,
                sizeof(B_LegoLab04_T));

  /* states (dwork) */
  (void) memset((void *)&LegoLab04_DW, 0,
                sizeof(DW_LegoLab04_T));

  /* Start for S-Function (ev3_ultrasonic_sensor): '<Root>/Ultrasonic Sensor' */
  initUltrasonicSensor(1U);

  /* Start for S-Function (ev3_lcd): '<S2>/LCD' */
  initLCD();

  /* Start for S-Function (ev3_encoder): '<Root>/Encoder1' */
  initEncoder(4U);

  /* Start for S-Function (ev3_lcd): '<S3>/LCD' */
  initLCD();

  /* Start for S-Function (ev3_motor): '<S4>/Motor' */
  initMotor(1U);

  /* Start for S-Function (ev3_motor): '<S5>/Motor' */
  initMotor(4U);

  /* Start for S-Function (ev3_encoder): '<Root>/Encoder' */
  initEncoder(1U);

  /* SystemInitialize for Chart: '<Root>/Chart' */
  LegoLab04_DW.temporalCounter_i1 = 0U;
  LegoLab04_DW.is_active_c3_LegoLab04 = 0U;
  LegoLab04_DW.is_c3_LegoLab04 = LegoLab04_IN_NO_ACTIVE_CHILD;
  LegoLab04_DW.V = 20.0;
}

/* Model terminate function */
void LegoLab04_terminate(void)
{
  /* Terminate for S-Function (ev3_ultrasonic_sensor): '<Root>/Ultrasonic Sensor' */
  terminateUltrasonicSensor(1U);

  /* Terminate for S-Function (ev3_lcd): '<S2>/LCD' */
  terminateLCD();

  /* Terminate for S-Function (ev3_encoder): '<Root>/Encoder1' */
  terminateEncoder(4U);

  /* Terminate for S-Function (ev3_lcd): '<S3>/LCD' */
  terminateLCD();

  /* Terminate for S-Function (ev3_motor): '<S4>/Motor' */
  terminateMotor(1U, 2U);

  /* Terminate for S-Function (ev3_motor): '<S5>/Motor' */
  terminateMotor(4U, 2U);

  /* Terminate for S-Function (ev3_encoder): '<Root>/Encoder' */
  terminateEncoder(1U);
}
