/*
 * LegoLab04_private.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "LegoLab04".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Apr 11 12:25:09 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LegoLab04_private_h_
#define RTW_HEADER_LegoLab04_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#endif                                 /* RTW_HEADER_LegoLab04_private_h_ */
