/*
 * LegoLab04_data.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "LegoLab04".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Apr 11 12:25:09 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "LegoLab04.h"
#include "LegoLab04_private.h"

/* Constant parameters (default storage) */
const ConstP_LegoLab04_T LegoLab04_ConstP = {
  /* Expression: lcdStr1
   * Referenced by: '<S2>/LCD'
   */
  { 83U, 101U, 110U, 115U, 111U, 114U, 0U },

  /* Expression: lcdStr1
   * Referenced by: '<S3>/LCD'
   */
  { 69U, 110U, 99U, 111U, 100U, 101U, 114U, 68U, 0U }
};
