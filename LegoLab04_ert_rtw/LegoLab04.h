/*
 * LegoLab04.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "LegoLab04".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Apr 11 12:25:09 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LegoLab04_h_
#define RTW_HEADER_LegoLab04_h_
#include <math.h>
#include <stddef.h>
#include <string.h>
#ifndef LegoLab04_COMMON_INCLUDES_
# define LegoLab04_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "driver_ev3.h"
#endif                                 /* LegoLab04_COMMON_INCLUDES_ */

#include "LegoLab04_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block signals (default storage) */
typedef struct {
  real_T M_left;                       /* '<Root>/Chart' */
  real_T M_right;                      /* '<Root>/Chart' */
} B_LegoLab04_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T V;                            /* '<Root>/Chart' */
  int32_T Reset;                       /* '<Root>/Chart' */
  uint8_T is_active_c3_LegoLab04;      /* '<Root>/Chart' */
  uint8_T is_c3_LegoLab04;             /* '<Root>/Chart' */
  uint8_T temporalCounter_i1;          /* '<Root>/Chart' */
} DW_LegoLab04_T;

/* Constant parameters (default storage) */
typedef struct {
  /* Expression: lcdStr1
   * Referenced by: '<S2>/LCD'
   */
  uint8_T LCD_p1[7];

  /* Expression: lcdStr1
   * Referenced by: '<S3>/LCD'
   */
  uint8_T LCD_p1_m[9];
} ConstP_LegoLab04_T;

/* Real-time Model Data Structure */
struct tag_RTM_LegoLab04_T {
  const char_T *errorStatus;
};

/* Block signals (default storage) */
extern B_LegoLab04_T LegoLab04_B;

/* Block states (default storage) */
extern DW_LegoLab04_T LegoLab04_DW;

/* Constant parameters (default storage) */
extern const ConstP_LegoLab04_T LegoLab04_ConstP;

/* Model entry point functions */
extern void LegoLab04_initialize(void);
extern void LegoLab04_step(void);
extern void LegoLab04_terminate(void);

/* Real-time Model object */
extern RT_MODEL_LegoLab04_T *const LegoLab04_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S3>/Data Type Conversion' : Eliminate redundant data type conversion
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LegoLab04'
 * '<S1>'   : 'LegoLab04/Chart'
 * '<S2>'   : 'LegoLab04/Display'
 * '<S3>'   : 'LegoLab04/Display1'
 * '<S4>'   : 'LegoLab04/Motor2'
 * '<S5>'   : 'LegoLab04/Motor3'
 */
#endif                                 /* RTW_HEADER_LegoLab04_h_ */
