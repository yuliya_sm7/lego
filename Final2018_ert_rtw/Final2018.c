/*
 * Final2018.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "Final2018".
 *
 * Model version              : 1.1
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Sat Apr 14 18:04:15 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Final2018.h"
#include "Final2018_private.h"

/* Named constants for Chart: '<Root>/Chart' */
#define Final2018_IN_Finish            ((uint8_T)1U)
#define Final2018_IN_Left              ((uint8_T)2U)
#define Final2018_IN_NO_ACTIVE_CHILD   ((uint8_T)0U)
#define Final2018_IN_Right             ((uint8_T)3U)
#define Final2018_IN_Rotate            ((uint8_T)4U)
#define Final2018_IN_Start             ((uint8_T)5U)
#define Final2018_IN_Vpered            ((uint8_T)6U)

/* Block signals (default storage) */
B_Final2018_T Final2018_B;

/* Block states (default storage) */
DW_Final2018_T Final2018_DW;

/* Real-time model */
RT_MODEL_Final2018_T Final2018_M_;
RT_MODEL_Final2018_T *const Final2018_M = &Final2018_M_;

/* Model step function */
void Final2018_step(void)
{
  int32_T rtb_Encoder1_0;
  int8_T tmp;
  uint8_T tmp_0;
  real_T tmp_1;
  int32_T rtb_Encoder1_1;

  /* S-Function (ev3_encoder): '<Root>/Encoder1' */
  rtb_Encoder1_0 = getEncoderValueNoReset(4U);

  /* FromWorkspace: '<Root>/From Workspace1' */
  {
    real_T *pDataValues = (real_T *) Final2018_DW.FromWorkspace1_PWORK.DataPtr;

    {
      int_T elIdx;
      for (elIdx = 0; elIdx < 7; ++elIdx) {
        (&Final2018_B.FromWorkspace1[0])[elIdx] = pDataValues[0];
        pDataValues += 1;
      }
    }
  }

  /* Chart: '<Root>/Chart' incorporates:
   *  S-Function (ev3_encoder): '<Root>/Encoder1'
   */
  if (Final2018_DW.temporalCounter_i1 < 15U) {
    Final2018_DW.temporalCounter_i1++;
  }

  /* Gateway: Chart */
  /* During: Chart */
  if (Final2018_DW.is_active_c3_Final2018 == 0U) {
    /* Entry: Chart */
    Final2018_DW.is_active_c3_Final2018 = 1U;

    /* Entry Internal: Chart */
    /* Transition: '<S1>:153' */
    Final2018_DW.is_c3_Final2018 = Final2018_IN_Start;
    Final2018_DW.temporalCounter_i1 = 0U;

    /* Entry 'Start': '<S1>:147' */
    /* '<S1>:147:1' V=-50; */
    Final2018_DW.V = -50.0;

    /* '<S1>:147:1' T=-50; */
    Final2018_DW.T = -50.0;

    /* '<S1>:147:1' Mot_A=0; */
    Final2018_B.Mot_A = 0.0;

    /* '<S1>:147:1' Mot_D=0; */
    Final2018_B.Mot_D = 0.0;

    /* '<S1>:147:3' i=1; */
    Final2018_DW.i = 1.0;

    /* '<S1>:147:4' value = 0; */
    Final2018_DW.value = 0.0;
  } else {
    switch (Final2018_DW.is_c3_Final2018) {
     case Final2018_IN_Finish:
      /* During 'Finish': '<S1>:164' */
      break;

     case Final2018_IN_Left:
      /* During 'Left': '<S1>:156' */
      /* '<S1>:161:1' sf_internal_predicateOutput = ... */
      /* '<S1>:161:1' (EncoderD - ResetD) > value; */
      if ((rtb_Encoder1_0 >= 0) && (Final2018_B.ResetD < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_1 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (Final2018_B.ResetD > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_1 = MIN_int32_T;
      } else {
        rtb_Encoder1_1 = rtb_Encoder1_0 - Final2018_B.ResetD;
      }

      if (rtb_Encoder1_1 > Final2018_DW.value) {
        /* Transition: '<S1>:161' */
        Final2018_DW.is_c3_Final2018 = Final2018_IN_Vpered;

        /* Entry 'Vpered': '<S1>:154' */
        /* '<S1>:154:1' Mot_A=V; */
        Final2018_B.Mot_A = Final2018_DW.V;

        /* '<S1>:154:1' Mot_D=V; */
        Final2018_B.Mot_D = Final2018_DW.V;

        /* '<S1>:154:1' ResetD = EncoderD; */
        Final2018_B.ResetD = rtb_Encoder1_0;

        /* '<S1>:154:3' value = v_out(i); */
        Final2018_DW.value = Final2018_B.FromWorkspace1[(int32_T)Final2018_DW.i
          - 1];

        /* '<S1>:154:5' i = i+1; */
        Final2018_DW.i++;
      }
      break;

     case Final2018_IN_Right:
      /* During 'Right': '<S1>:157' */
      /* '<S1>:162:1' sf_internal_predicateOutput = ... */
      /* '<S1>:162:1' (EncoderD - ResetD) > -value; */
      if ((rtb_Encoder1_0 >= 0) && (Final2018_B.ResetD < rtb_Encoder1_0 -
           MAX_int32_T)) {
        rtb_Encoder1_1 = MAX_int32_T;
      } else if ((rtb_Encoder1_0 < 0) && (Final2018_B.ResetD > rtb_Encoder1_0 -
                  MIN_int32_T)) {
        rtb_Encoder1_1 = MIN_int32_T;
      } else {
        rtb_Encoder1_1 = rtb_Encoder1_0 - Final2018_B.ResetD;
      }

      if (rtb_Encoder1_1 > -Final2018_DW.value) {
        /* Transition: '<S1>:162' */
        Final2018_DW.is_c3_Final2018 = Final2018_IN_Vpered;

        /* Entry 'Vpered': '<S1>:154' */
        /* '<S1>:154:1' Mot_A=V; */
        Final2018_B.Mot_A = Final2018_DW.V;

        /* '<S1>:154:1' Mot_D=V; */
        Final2018_B.Mot_D = Final2018_DW.V;

        /* '<S1>:154:1' ResetD = EncoderD; */
        Final2018_B.ResetD = rtb_Encoder1_0;

        /* '<S1>:154:3' value = v_out(i); */
        Final2018_DW.value = Final2018_B.FromWorkspace1[(int32_T)Final2018_DW.i
          - 1];

        /* '<S1>:154:5' i = i+1; */
        Final2018_DW.i++;
      }
      break;

     case Final2018_IN_Rotate:
      /* During 'Rotate': '<S1>:146' */
      /* '<S1>:170:1' sf_internal_predicateOutput = ... */
      /* '<S1>:170:1' i > length(v_out); */
      if (Final2018_DW.i > 7.0) {
        /* Transition: '<S1>:170' */
        Final2018_DW.is_c3_Final2018 = Final2018_IN_Finish;

        /* Entry 'Finish': '<S1>:164' */
        /* '<S1>:164:1' Mot_A=0; */
        Final2018_B.Mot_A = 0.0;

        /* '<S1>:164:1' Mot_D=0; */
        Final2018_B.Mot_D = 0.0;
      } else {
        /* '<S1>:158:1' sf_internal_predicateOutput = ... */
        /* '<S1>:158:1' value >= 0; */
        if (Final2018_DW.value >= 0.0) {
          /* Transition: '<S1>:158' */
          Final2018_DW.is_c3_Final2018 = Final2018_IN_Left;

          /* Entry 'Left': '<S1>:156' */
          /* '<S1>:156:1' ResetD = EncoderD; */
          Final2018_B.ResetD = rtb_Encoder1_0;

          /* '<S1>:156:1' Mot_A=-T; */
          Final2018_B.Mot_A = -Final2018_DW.T;

          /* '<S1>:156:1' Mot_D=T; */
          Final2018_B.Mot_D = Final2018_DW.T;
        } else {
          /* '<S1>:159:1' sf_internal_predicateOutput = ... */
          /* '<S1>:159:1' value < 0; */
          if (Final2018_DW.value < 0.0) {
            /* Transition: '<S1>:159' */
            Final2018_DW.is_c3_Final2018 = Final2018_IN_Right;

            /* Entry 'Right': '<S1>:157' */
            /* '<S1>:157:1' ResetD = EncoderD; */
            Final2018_B.ResetD = rtb_Encoder1_0;

            /* '<S1>:157:1' Mot_A=T; */
            Final2018_B.Mot_A = Final2018_DW.T;

            /* '<S1>:157:1' Mot_D=-T; */
            Final2018_B.Mot_D = -Final2018_DW.T;
          }
        }
      }
      break;

     case Final2018_IN_Start:
      /* During 'Start': '<S1>:147' */
      /* '<S1>:150:1' sf_internal_predicateOutput = ... */
      /* '<S1>:150:1' after(1, sec); */
      if (Final2018_DW.temporalCounter_i1 >= 10U) {
        /* Transition: '<S1>:150' */
        Final2018_DW.is_c3_Final2018 = Final2018_IN_Rotate;

        /* Entry 'Rotate': '<S1>:146' */
        /* '<S1>:146:1' value = v_out(i); */
        Final2018_DW.value = Final2018_B.FromWorkspace1[(int32_T)Final2018_DW.i
          - 1];

        /* '<S1>:146:1' i = i+1; */
        Final2018_DW.i++;
      }
      break;

     default:
      /* During 'Vpered': '<S1>:154' */
      /* '<S1>:171:1' sf_internal_predicateOutput = ... */
      /* '<S1>:171:1' i > length(v_out); */
      if (Final2018_DW.i > 7.0) {
        /* Transition: '<S1>:171' */
        Final2018_DW.is_c3_Final2018 = Final2018_IN_Finish;

        /* Entry 'Finish': '<S1>:164' */
        /* '<S1>:164:1' Mot_A=0; */
        Final2018_B.Mot_A = 0.0;

        /* '<S1>:164:1' Mot_D=0; */
        Final2018_B.Mot_D = 0.0;
      } else {
        /* '<S1>:155:1' sf_internal_predicateOutput = ... */
        /* '<S1>:155:1' (ResetD-EncoderD)  > value; */
        if ((Final2018_B.ResetD >= 0) && (rtb_Encoder1_0 < Final2018_B.ResetD -
             MAX_int32_T)) {
          rtb_Encoder1_0 = MAX_int32_T;
        } else if ((Final2018_B.ResetD < 0) && (rtb_Encoder1_0 >
                    Final2018_B.ResetD - MIN_int32_T)) {
          rtb_Encoder1_0 = MIN_int32_T;
        } else {
          rtb_Encoder1_0 = Final2018_B.ResetD - rtb_Encoder1_0;
        }

        if (rtb_Encoder1_0 > Final2018_DW.value) {
          /* Transition: '<S1>:155' */
          Final2018_DW.is_c3_Final2018 = Final2018_IN_Rotate;

          /* Entry 'Rotate': '<S1>:146' */
          /* '<S1>:146:1' value = v_out(i); */
          Final2018_DW.value = Final2018_B.FromWorkspace1[(int32_T)
            Final2018_DW.i - 1];

          /* '<S1>:146:1' i = i+1; */
          Final2018_DW.i++;
        }
      }
      break;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* S-Function (ev3_lcd): '<S2>/LCD' */
  tmp_0 = 0U;
  lcdDisplay(Final2018_B.ResetD, &tmp_0, 1U, 1U);

  /* DataTypeConversion: '<S4>/Data Type Conversion' */
  tmp_1 = floor(Final2018_B.Mot_D);
  if (tmp_1 < 128.0) {
    if (tmp_1 >= -128.0) {
      /* S-Function (ev3_motor): '<S4>/Motor' */
      tmp = (int8_T)tmp_1;
    } else {
      /* S-Function (ev3_motor): '<S4>/Motor' */
      tmp = MIN_int8_T;
    }
  } else {
    /* S-Function (ev3_motor): '<S4>/Motor' */
    tmp = MAX_int8_T;
  }

  /* End of DataTypeConversion: '<S4>/Data Type Conversion' */

  /* S-Function (ev3_motor): '<S4>/Motor' */
  setMotor(&tmp, 1U, 2U);

  /* DataTypeConversion: '<S5>/Data Type Conversion' */
  tmp_1 = floor(Final2018_B.Mot_A);
  if (tmp_1 < 128.0) {
    if (tmp_1 >= -128.0) {
      /* S-Function (ev3_motor): '<S5>/Motor' */
      tmp = (int8_T)tmp_1;
    } else {
      /* S-Function (ev3_motor): '<S5>/Motor' */
      tmp = MIN_int8_T;
    }
  } else {
    /* S-Function (ev3_motor): '<S5>/Motor' */
    tmp = MAX_int8_T;
  }

  /* End of DataTypeConversion: '<S5>/Data Type Conversion' */

  /* S-Function (ev3_motor): '<S5>/Motor' */
  setMotor(&tmp, 4U, 2U);

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++Final2018_M->Timing.clockTick0)) {
    ++Final2018_M->Timing.clockTickH0;
  }

  Final2018_M->Timing.t[0] = Final2018_M->Timing.clockTick0 *
    Final2018_M->Timing.stepSize0 + Final2018_M->Timing.clockTickH0 *
    Final2018_M->Timing.stepSize0 * 4294967296.0;

  {
    /* Update absolute timer for sample time: [0.1s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The resolution of this integer timer is 0.1, which is the step size
     * of the task. Size of "clockTick1" ensures timer will not overflow during the
     * application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    Final2018_M->Timing.clockTick1++;
    if (!Final2018_M->Timing.clockTick1) {
      Final2018_M->Timing.clockTickH1++;
    }
  }
}

/* Model initialize function */
void Final2018_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)Final2018_M, 0,
                sizeof(RT_MODEL_Final2018_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&Final2018_M->solverInfo,
                          &Final2018_M->Timing.simTimeStep);
    rtsiSetTPtr(&Final2018_M->solverInfo, &rtmGetTPtr(Final2018_M));
    rtsiSetStepSizePtr(&Final2018_M->solverInfo, &Final2018_M->Timing.stepSize0);
    rtsiSetErrorStatusPtr(&Final2018_M->solverInfo, (&rtmGetErrorStatus
      (Final2018_M)));
    rtsiSetRTModelPtr(&Final2018_M->solverInfo, Final2018_M);
  }

  rtsiSetSimTimeStep(&Final2018_M->solverInfo, MAJOR_TIME_STEP);
  rtsiSetSolverName(&Final2018_M->solverInfo,"FixedStepDiscrete");
  rtmSetTPtr(Final2018_M, &Final2018_M->Timing.tArray[0]);
  Final2018_M->Timing.stepSize0 = 0.1;

  /* block I/O */
  (void) memset(((void *) &Final2018_B), 0,
                sizeof(B_Final2018_T));

  /* states (dwork) */
  (void) memset((void *)&Final2018_DW, 0,
                sizeof(DW_Final2018_T));

  /* Start for S-Function (ev3_encoder): '<Root>/Encoder1' */
  initEncoder(4U);

  /* Start for FromWorkspace: '<Root>/From Workspace1' */
  {
    static real_T pTimeValues0[] = { 0.0 } ;

    static real_T pDataValues0[] = { -701.81217339632394, 4123.1056256176607,
      701.81217339632394, 21000.0, 1090.0704743175907, 5385.1648071345044, 0.0 }
    ;

    Final2018_DW.FromWorkspace1_PWORK.TimePtr = (void *) pTimeValues0;
    Final2018_DW.FromWorkspace1_PWORK.DataPtr = (void *) pDataValues0;
    Final2018_DW.FromWorkspace1_IWORK.PrevIndex = 0;
  }

  /* Start for S-Function (ev3_lcd): '<S2>/LCD' */
  initLCD();

  /* Start for S-Function (ev3_motor): '<S4>/Motor' */
  initMotor(1U);

  /* Start for S-Function (ev3_motor): '<S5>/Motor' */
  initMotor(4U);

  /* SystemInitialize for Chart: '<Root>/Chart' */
  Final2018_DW.temporalCounter_i1 = 0U;
  Final2018_DW.is_active_c3_Final2018 = 0U;
  Final2018_DW.is_c3_Final2018 = Final2018_IN_NO_ACTIVE_CHILD;
}

/* Model terminate function */
void Final2018_terminate(void)
{
  /* Terminate for S-Function (ev3_encoder): '<Root>/Encoder1' */
  terminateEncoder(4U);

  /* Terminate for S-Function (ev3_lcd): '<S2>/LCD' */
  terminateLCD();

  /* Terminate for S-Function (ev3_motor): '<S4>/Motor' */
  terminateMotor(1U, 2U);

  /* Terminate for S-Function (ev3_motor): '<S5>/Motor' */
  terminateMotor(4U, 2U);
}
