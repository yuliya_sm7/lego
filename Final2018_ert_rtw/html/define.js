function CodeDefine() { 
this.def = new Array();
this.def["stopRequested"] = {file: "ert_main_c.html",line:19,type:"var"};
this.def["runModel"] = {file: "ert_main_c.html",line:20,type:"var"};
this.def["stopSem"] = {file: "ert_main_c.html",line:21,type:"var"};
this.def["baserateTaskSem"] = {file: "ert_main_c.html",line:22,type:"var"};
this.def["schedulerThread"] = {file: "ert_main_c.html",line:23,type:"var"};
this.def["baseRateThread"] = {file: "ert_main_c.html",line:24,type:"var"};
this.def["threadJoinStatus"] = {file: "ert_main_c.html",line:25,type:"var"};
this.def["terminatingmodel"] = {file: "ert_main_c.html",line:26,type:"var"};
this.def["baseRateTask"] = {file: "ert_main_c.html",line:27,type:"fcn"};
this.def["exitFcn"] = {file: "ert_main_c.html",line:46,type:"fcn"};
this.def["terminateTask"] = {file: "ert_main_c.html",line:52,type:"fcn"};
this.def["main"] = {file: "ert_main_c.html",line:71,type:"fcn"};
this.def["Final2018_B"] = {file: "Final2018_c.html",line:34,type:"var"};
this.def["Final2018_DW"] = {file: "Final2018_c.html",line:37,type:"var"};
this.def["Final2018_M_"] = {file: "Final2018_c.html",line:40,type:"var"};
this.def["Final2018_M"] = {file: "Final2018_c.html",line:41,type:"var"};
this.def["Final2018_step"] = {file: "Final2018_c.html",line:44,type:"fcn"};
this.def["Final2018_initialize"] = {file: "Final2018_c.html",line:378,type:"fcn"};
this.def["Final2018_terminate"] = {file: "Final2018_c.html",line:442,type:"fcn"};
this.def["B_Final2018_T"] = {file: "Final2018_h.html",line:62,type:"type"};
this.def["DW_Final2018_T"] = {file: "Final2018_h.html",line:83,type:"type"};
this.def["RT_MODEL_Final2018_T"] = {file: "Final2018_types_h.html",line:25,type:"type"};
this.def["long_T"] = {file: "multiword_types_h.html",line:28,type:"type"};
this.def["int64m_T"] = {file: "multiword_types_h.html",line:35,type:"type"};
this.def["cint64m_T"] = {file: "multiword_types_h.html",line:40,type:"type"};
this.def["uint64m_T"] = {file: "multiword_types_h.html",line:44,type:"type"};
this.def["cuint64m_T"] = {file: "multiword_types_h.html",line:49,type:"type"};
this.def["int96m_T"] = {file: "multiword_types_h.html",line:53,type:"type"};
this.def["cint96m_T"] = {file: "multiword_types_h.html",line:58,type:"type"};
this.def["uint96m_T"] = {file: "multiword_types_h.html",line:62,type:"type"};
this.def["cuint96m_T"] = {file: "multiword_types_h.html",line:67,type:"type"};
this.def["int128m_T"] = {file: "multiword_types_h.html",line:71,type:"type"};
this.def["cint128m_T"] = {file: "multiword_types_h.html",line:76,type:"type"};
this.def["uint128m_T"] = {file: "multiword_types_h.html",line:80,type:"type"};
this.def["cuint128m_T"] = {file: "multiword_types_h.html",line:85,type:"type"};
this.def["int160m_T"] = {file: "multiword_types_h.html",line:89,type:"type"};
this.def["cint160m_T"] = {file: "multiword_types_h.html",line:94,type:"type"};
this.def["uint160m_T"] = {file: "multiword_types_h.html",line:98,type:"type"};
this.def["cuint160m_T"] = {file: "multiword_types_h.html",line:103,type:"type"};
this.def["int192m_T"] = {file: "multiword_types_h.html",line:107,type:"type"};
this.def["cint192m_T"] = {file: "multiword_types_h.html",line:112,type:"type"};
this.def["uint192m_T"] = {file: "multiword_types_h.html",line:116,type:"type"};
this.def["cuint192m_T"] = {file: "multiword_types_h.html",line:121,type:"type"};
this.def["int224m_T"] = {file: "multiword_types_h.html",line:125,type:"type"};
this.def["cint224m_T"] = {file: "multiword_types_h.html",line:130,type:"type"};
this.def["uint224m_T"] = {file: "multiword_types_h.html",line:134,type:"type"};
this.def["cuint224m_T"] = {file: "multiword_types_h.html",line:139,type:"type"};
this.def["int256m_T"] = {file: "multiword_types_h.html",line:143,type:"type"};
this.def["cint256m_T"] = {file: "multiword_types_h.html",line:148,type:"type"};
this.def["uint256m_T"] = {file: "multiword_types_h.html",line:152,type:"type"};
this.def["cuint256m_T"] = {file: "multiword_types_h.html",line:157,type:"type"};
this.def["pointer_T"] = {file: "rtwtypes_h.html",line:28,type:"type"};
}
CodeDefine.instance = new CodeDefine();
var testHarnessInfo = {OwnerFileName: "", HarnessOwner: "", HarnessName: "", IsTestHarness: "0"};
var relPathToBuildDir = "../ert_main.c";
var fileSep = "\\";
var isPC = true;
function Html2SrcLink() {
	this.html2SrcPath = new Array;
	this.html2Root = new Array;
	this.html2SrcPath["ert_main_c.html"] = "../ert_main.c";
	this.html2Root["ert_main_c.html"] = "ert_main_c.html";
	this.html2SrcPath["Final2018_c.html"] = "../Final2018.c";
	this.html2Root["Final2018_c.html"] = "Final2018_c.html";
	this.html2SrcPath["Final2018_h.html"] = "../Final2018.h";
	this.html2Root["Final2018_h.html"] = "Final2018_h.html";
	this.html2SrcPath["Final2018_private_h.html"] = "../Final2018_private.h";
	this.html2Root["Final2018_private_h.html"] = "Final2018_private_h.html";
	this.html2SrcPath["Final2018_types_h.html"] = "../Final2018_types.h";
	this.html2Root["Final2018_types_h.html"] = "Final2018_types_h.html";
	this.html2SrcPath["multiword_types_h.html"] = "../multiword_types.h";
	this.html2Root["multiword_types_h.html"] = "multiword_types_h.html";
	this.html2SrcPath["rtwtypes_h.html"] = "../rtwtypes.h";
	this.html2Root["rtwtypes_h.html"] = "rtwtypes_h.html";
	this.html2SrcPath["rtmodel_h.html"] = "../rtmodel.h";
	this.html2Root["rtmodel_h.html"] = "rtmodel_h.html";
	this.html2SrcPath["MW_custom_RTOS_header_h.html"] = "../MW_custom_RTOS_header.h";
	this.html2Root["MW_custom_RTOS_header_h.html"] = "MW_custom_RTOS_header_h.html";
	this.html2SrcPath["MW_target_hardware_resources_h.html"] = "../MW_target_hardware_resources.h";
	this.html2Root["MW_target_hardware_resources_h.html"] = "MW_target_hardware_resources_h.html";
	this.getLink2Src = function (htmlFileName) {
		 if (this.html2SrcPath[htmlFileName])
			 return this.html2SrcPath[htmlFileName];
		 else
			 return null;
	}
	this.getLinkFromRoot = function (htmlFileName) {
		 if (this.html2Root[htmlFileName])
			 return this.html2Root[htmlFileName];
		 else
			 return null;
	}
}
Html2SrcLink.instance = new Html2SrcLink();
var fileList = [
"ert_main_c.html","Final2018_c.html","Final2018_h.html","Final2018_private_h.html","Final2018_types_h.html","multiword_types_h.html","rtwtypes_h.html","rtmodel_h.html","MW_custom_RTOS_header_h.html","MW_target_hardware_resources_h.html"];
