/*
 * Final2018_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "Final2018".
 *
 * Model version              : 1.1
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Sat Apr 14 18:04:15 2018
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Final2018_types_h_
#define RTW_HEADER_Final2018_types_h_

/* Forward declaration for rtModel */
typedef struct tag_RTM_Final2018_T RT_MODEL_Final2018_T;

#endif                                 /* RTW_HEADER_Final2018_types_h_ */
